# Factory presentation/Présentation de la fabrique
**[Demo](https://antoinentl.gitlab.io/fabrique/)**

## Fabrique de publication littéraire
_English below._

Ce dépôt rassemble les expérimentations d'Antoine Fauchié pour constituer une fabrique de publication littéraire, destinée à produire différents artefacts à partir d'une même source.
Sources au format Markdown, formats de fichiers produits : HTML (version web), HTML (version imprimable) et EPUB.
Prototype non finalisé développé dans le cadre de la thèse de doctorat d'Antoine Fauchié ([www.quaternum.net](https://www.quaternum.net)).

## Literary publication factory
This repository gathers Antoine Fauchié's experiments to constitute a literary publication factory, intended to produce different artifacts from the same source.
Sources in Markdown format, produced file formats: HTML (web version), HTML (printable version) and EPUB.
Unfinished prototype developed in the framework of Antoine Fauchié's PhD thesis ([www.quaternum.net](https://www.quaternum.net)).

## Programs and software used

- [Hugo](https://gohugo.io/): a static site generator
- [Hitchens](https://github.com/patdryburgh/hitchens): a theme developed by Pat Dryburgh initially for Jekyll
- [Epub Theme](https://github.com/weitblick/epub): a theme developed by Erhard Maria Klein
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/): a continuous integration/development tool and platform developed and hosted by GitLab

This factory can be easily connected with a headless CMS like [Forestry](https://forestry.io/) for managing the content with a graphical interface.