---
title: About
date: 2021-02-06
weight: 50
type: colophon
---
Demo website for a _literary publication factory_ project, in the context of the doctoral thesis of Antoine Fauchié ([www.quaternum.net](https://www.quaternum.net)): [https://gitlab.com/antoinentl/fabrique](https://gitlab.com/antoinentl/fabrique).

Source of the text: [Wikisource](https://en.wikisource.org/wiki/Anarchism_and_Other_Essays).